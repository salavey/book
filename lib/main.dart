import "dart:io";
import "package:flutter/material.dart";
import "package:path_provider/path_provider.dart";
//import "appointments/Appointments.dart";
//import "contacts/Contacts.dart";
//import "notes/notes.dart";
//import "utils.dart" as utils;

void main() {
  startMeUp() async {
    //Directory? docsDir = await getApplicationDocumentsDirectory();
    //print(docsDir);
    //utils.docsDir = docsDir;
    runApp(const FlutterBook());
  }
  startMeUp();
}

class FlutterBook extends StatelessWidget {
  const FlutterBook({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    print("## FlutterBook.build()");

    return MaterialApp(
        home : DefaultTabController(
            length : 4,
            child : Scaffold(
                appBar : AppBar(
                    title : const Text("FlutterBook"),
                    bottom : const TabBar(
                        tabs : [
                          Tab(icon : Icon(Icons.date_range), text : "Appointments"),
                          Tab(icon : Icon(Icons.contacts), text : "Contacts"),
                          Tab(icon : Icon(Icons.note), text : "Notes"),
                          Tab(icon : Icon(Icons.assignment_turned_in), text : "Tasks")
                        ] /* End TabBar.tabs. */
                    ) /* End TabBar. */
                ), /* End AppBar. */
                body : const TabBarView(
                    children : [
                      Text('1111'),
                      Text('2222'),
                      Text('3333'),
                      Text('4444'),
                      //Appointments(),
                      //Contacts(),
                      //Tasks()
                    ] /* End TabBarView.children. */
                ) /* End TabBarView. */
            ) /* End Scaffold. */
        ) /* End DefaultTabController. */
    ); /* End MaterialApp. */

  } /* End build(). */
}